import { defineConfig } from "astro/config";

import node from "@astrojs/node";

// https://astro.build/config
export default defineConfig({
  output: "server",
  // eslint-disable-next-line no-undef
  server: { port: +(process.env.PORT ?? 4321) },
  adapter: node({ mode: "standalone" }),
});
