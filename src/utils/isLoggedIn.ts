import type { AstroCookies } from "astro";
import queryBackend from "~/utils/queryBackend";

type AuthQueryRes = {
  member?: { spotifyId?: string } | undefined;
} & Record<string, unknown>;

const isLoggedIn = async (cookies: AstroCookies) => {
  const data = await queryBackend<AuthQueryRes>(
    "query currentUser { member { spotifyId } }",
    cookies,
  );

  const member = data?.member;

  if (!member?.spotifyId) {
    return false;
  }
  return true;
};

export default isLoggedIn;
