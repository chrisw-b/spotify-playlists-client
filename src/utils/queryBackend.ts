/// <reference types="vite/client" />

import type { AstroCookies } from "astro";

const queryBackend = async <T extends Record<string, unknown>>(
  query: string,
  cookies: AstroCookies,
): Promise<T | null> => {
  const queryUrl =
    process.env.GRAPHQL_ENDPOINT ?? import.meta.env.GRAPHQL_ENDPOINT;
  const token = cookies.get("connect.sid");
  try {
    const response = await fetch(queryUrl, {
      method: "POST",
      headers: new Headers({
        cookie: `connect.sid=${token?.value}`,
        "Content-Type": "application/json",
      }),
      credentials: "include",
      body: JSON.stringify({ query }),
    });
    if (!response.ok) {
      if (response.status === 401) {
        return null;
      }
      throw new Error(
        `Failed to fetch: ${response.status} - ${response.statusText}`,
      );
    }
    const json: { data: T } = await response.json();
    return json?.data;
  } catch (e) {
    console.error(e);
    return null;
  }
};

export default queryBackend;
