/// <reference types="astro/client" />
interface ImportMetaEnv {
  GRAPHQL_ENDPOINT: string;
  AUTH_ENDPOINT: string;
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
