import type { APIRoute } from "astro";
export const GET: APIRoute = async ({ cookies, redirect }) => {
  const queryUrl =
    process.env.GRAPHQL_ENDPOINT ?? import.meta.env.GRAPHQL_ENDPOINT;

  const token = cookies.get("connect.sid");

  const response = await fetch(queryUrl, {
    method: "POST",
    headers: new Headers({
      cookie: `connect.sid=${token?.value}`,
      "Content-Type": "application/json",
    }),
    credentials: "include",
    body: JSON.stringify({
      query: "mutation logout { logout { success } }",
    }),
  });
  if (!response.ok) {
    return new Response(null, response);
  }

  return redirect("/login");
};
