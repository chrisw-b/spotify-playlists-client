import type { APIRoute } from "astro";
export const GET: APIRoute = ({ redirect }) => {
  return redirect(process.env.AUTH_ENDPOINT ?? import.meta.env.AUTH_ENDPOINT);
};
