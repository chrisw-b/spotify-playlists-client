import type { APIRoute } from "astro";
import type { Period } from "lastfm-njs";
export const POST: APIRoute = async ({ request, redirect, cookies }) => {
  const queryUrl =
    process.env.GRAPHQL_ENDPOINT ?? import.meta.env.GRAPHQL_ENDPOINT;

  const data = await request.formData();
  const token = cookies.get("connect.sid");

  const type = data.get("type") as "mostPlayed" | "recentlyAdded" | null;
  const enabled = data.get("enabled")?.toString().toLocaleLowerCase() === "on";
  const length = Number(data.get("length") as string | null);
  const lastfm = data.get("lastfm") as string | null;
  const period = data.get("period") as Period | null;

  if (!type) {
    return redirect("/?update=error", 303);
  }
  try {
    const response = await fetch(queryUrl, {
      method: "POST",
      headers: new Headers({
        cookie: `connect.sid=${token?.value}`,
        "Content-Type": "application/json",
      }),
      credentials: "include",
      body: JSON.stringify({
        variables: { settings: { enabled, length, lastfm, period } },
        query: `
        mutation Update($settings: updatePlaylistType!){ 
          updatePlaylist(playlistKind: ${type} patch: $settings) {
            spotifyId
          }
        }`,
      }),
    });
    if (!response.ok) {
      throw new Error(response.statusText);
    }

    return redirect("/?update=success", 303);
  } catch (_e) {
    return redirect("/?update=error", 303);
  }
};
